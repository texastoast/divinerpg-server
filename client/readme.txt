First, install Minecraft Forge by running forge-1.6.4-9...-installer.jar (double-click) and choosing the client installation.

Next, run Minecraft using the new "Forge" profile.  Once it's running, go ahead and exit.

Next, unzip the contents of divinerpg-plus into the 'mods' folder in your Minecraft library.